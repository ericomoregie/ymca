<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_No Thank You - Please Dont Show Again</name>
   <tag></tag>
   <elementGuidId>c8ec761a-a9f1-4183-ad99-6d69e4616b5e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='wisepop-192752']/div[2]/div/div/div/div[2]/div[3]/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>DismissPopupBlock__Button-ubmctd-1 jLxBDy</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>No, Thank You - Please Don't Show Again</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wisepop-192752&quot;)/div[@class=&quot;Popup__WisepopScrollable-sc-1jdod1q-0 WVKkg&quot;]/div[@class=&quot;Popup__WisepopContent-sc-1jdod1q-3 hRHebs wisepops-content&quot;]/div[@class=&quot;VideoStopper__Container-jc37qi-0 gPPtAz&quot;]/div[@class=&quot;BlocksContainer-sc-3tl3gf-0 nJHvL wisepops-column&quot;]/div[@class=&quot;wisepops-blocks-wrapper&quot;]/div[@class=&quot;BlockReadOnly__BlockContainerReadOnly-sc-1sk33k3-0 fRZmjG&quot;]/div[@class=&quot;DismissPopupBlock__Container-ubmctd-0 hWoFRQ wisepops-block-dismiss wisepops-block-id-865111&quot;]/button[@class=&quot;DismissPopupBlock__Button-ubmctd-1 jLxBDy&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='wisepop-192752']/div[2]/div/div/div/div[2]/div[3]/div/button</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='DONATE NOW'])[1]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::button[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/button</value>
   </webElementXpaths>
</WebElementEntity>
