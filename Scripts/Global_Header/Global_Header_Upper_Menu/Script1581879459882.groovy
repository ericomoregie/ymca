import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.home_page_url)

WebUI.waitForPageLoad(5, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Global_Header_Upper_Menu/button_No Thank You - Please Dont Show Again'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.maximizeWindow()

WebUI.executeJavaScript('return document.getElementById(\'header-bar\');', null)

if (header == 'header-bar') {
    WebUI.click(findTestObject('Global_Header_Upper_Menu/a_Espaol'))

    WebUI.click(findTestObject('Global_Header_Upper_Menu/a_English'))
}

WebUI.click(findTestObject('Global_Header_Upper_Menu/a_Y Locations'))

sYLocationsText = WebUI.getText(findTestObject('Global_Header_Upper_Menu/h2_Y Locations'))

WebUI.verifyMatch(sYLocationsText, 'Y LOCATIONS', false)

WebUI.click(findTestObject('Global_Header_Upper_Menu/img_Skip to main content_img-responsive visible-lg'))

WebUI.click(findTestObject('Global_Header_Upper_Menu/a_Contact Us'))

WebUI.verifyMatch('Contact Us', 'Contact Us', false)

WebUI.click(findTestObject('Global_Header_Upper_Menu/img_Skip to main content_img-responsive visible-lg (1)'))

WebUI.click(findTestObject('Global_Header_Upper_Menu/a_Donate'))

WebUI.verifyMatch('Donate Today', 'Donate Today', false)

WebUI.click(findTestObject('Global_Header_Upper_Menu/img_Skip to main content_img-responsive visible-lg (2)'))

WebUI.click(findTestObject('Global_Header_Upper_Menu/a_Search'))

WebUI.verifyMatch('Search', 'Search', false)

WebUI.click(findTestObject('Global_Header_Upper_Menu/img_Skip to main content_img-responsive visible-lg (3)'))

WebUI.click(findTestObject('Global_Header_Upper_Menu/a_Log In'))

WebUI.verifyMatch('My Y Login', 'My Y Login', false)

WebUI.click(findTestObject('Global_Header_Upper_Menu/img_Skip to main content_img-responsive visible-lg (4)'))

WebUI.closeBrowser()

