import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://staging.ymcasd.org/')

WebUI.waitForPageLoad(5)

not_run: WebUI.maximizeWindow()

not_run: WebUI.executeJavaScript('return document.getElementById(\'header-bar\');', [])

if (header == 'header-bar') {
    not_run: WebUI.click(findTestObject('Global_Header_Upper_Menu/a_Espaol'))

    not_run: WebUI.click(findTestObject('Global_Header_Upper_Menu/a_English'))

    WebUI.click(findTestObject('Global_Header_Upper_Menu/a_Y Locations'))
}

WebUI.verifyMatch('Y Locations', 'Y Locations', false)

WebUI.clickImage(findTestObject('Global_Header_Upper_Menu/img_Skip to main content_img-responsive visible-lg'))

WebUI.click(findTestObject('Global_Header_Upper_Menu/a_Contact Us'))

WebUI.verifyMatch('Contact Us', 'Contact Us', false)

WebUI.clickImage(findTestObject('Global_Header_Upper_Menu/img_Skip to main content_img-responsive visible-lg (1)'))

WebUI.click(findTestObject('Global_Header_Upper_Menu/a_Donate'))

WebUI.verifyMatch('Donate', 'Donate', false)

WebUI.clickImage(findTestObject('Global_Header_Upper_Menu/img_Skip to main content_img-responsive visible-lg (2)'))

WebUI.click(findTestObject('Global_Header_Upper_Menu/a_Search'))

WebUI.verifyMatch('Search', 'Search', false)

WebUI.click(findTestObject('Global_Header_Upper_Menu/img_Skip to main content_img-responsive visible-lg (3)'))

WebUI.click(findTestObject('Global_Header_Upper_Menu/a_Log In'))

WebUI.verifyMatch('My Y Login', 'My Y Login', false)

WebUI.click(findTestObject('Global_Header_Upper_Menu/img_Skip to main content_img-responsive visible-lg (4)'))

WebUI.closeBrowser()

